//
//  ViewController.swift
//  RIB
//
//  Created by Apple on 11/6/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var imageCache = NSCache<AnyObject, AnyObject>()

    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadImage(urlString: "https://m.media-amazon.com/images/M/MV5BNzhlYjE5MjMtZDJmYy00MGZmLTgwN2MtZGM0NTk2ZTczNmU5XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg")

        // Do any additional setup after loading the view.
    }
    
    func loadImage(urlString: String) {
    
        if let cacheImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.imageView.image =  cacheImage
                   return
        }
        
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) {(data,response,error) in
            
            if let error = error{
                
                print("Can not load data ")
                
            }
            guard let data =  data else {return}
            let image  = UIImage(data: data)
            self.imageCache.setObject(image!, forKey: urlString as AnyObject)
            DispatchQueue.main.async {
                self.imageView.image = image
            }
            
            
        }.resume()
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
